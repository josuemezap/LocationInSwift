//
//  ViewController.swift
//  locationTest
//
//  Created by Josue Meza Peña on 07-07-15.
//  Copyright (c) 2015 Josue Meza Peña. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    
    let locationManager: CLLocationManager = CLLocationManager()
//    let imageName = "beach"
//    let imageName = "360_1.jpg"
    let imageName = "360_2.jpg"
    
    var phoneWidth: Float!
    var frameWidth: Float!
    var frameHeight: Float!
    var frameCenter: Float!
    var xPosition: Float!
    var panoramicImage: UIImageView!
    var pinchEvent: UIPinchGestureRecognizer!
    var panEvent: UIPanGestureRecognizer!
    
    var latitude: Float = 0.0
    var longitude: Float = 0.0
    var useCompass = false
    
    var zoom: CGFloat = 1.0
    
    var imagePickerController: UIImagePickerController!
    
    @IBOutlet weak var locationStatusLabel: UILabel!
    @IBOutlet weak var locationHeadingLabel: UILabel!
    @IBOutlet weak var locationPositionLabel: UILabel!
    @IBOutlet weak var locationDiference: UILabel!
    
    func setXPosition(xPosition: Float) {
        self.panoramicImage.frame = CGRectMake(CGFloat(xPosition), 0, CGFloat(self.frameWidth), self.panoramicImage.frame.size.height)
        self.xPosition = xPosition
    }
    
    func getDisplacement(angle: Float) -> Float {
        return -((angle * frameWidth) / 360)
    }
    
    func goToAngle(angle: Float) {
        var a = angle >= 180 ? 360 - angle : angle
        var d = self.getDisplacement(a)
        var xPosition = self.frameCenter + (angle >= 180 ? -d : d)
        self.setXPosition(xPosition)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set image data
        self.phoneWidth = Float(UIScreen.mainScreen().bounds.size.height)
        let image = UIImage(named: self.imageName)
        self.panoramicImage = UIImageView(image: image)
        self.panoramicImage.contentMode = .ScaleAspectFit
        
        self.frameHeight = Float(UIScreen.mainScreen().bounds.size.height)
        self.frameWidth = Float((image!.size.width * CGFloat(self.frameHeight)) / image!.size.height)
        self.panoramicImage.frame.size.width = CGFloat(self.frameWidth)
        self.panoramicImage.frame.size.height = CGFloat(self.frameHeight)
        
        self.frameCenter = -((self.frameWidth / 2.0) - (Float(UIScreen.mainScreen().bounds.size.width) / 2.0))
        self.setXPosition(self.frameCenter)
        self.view.addSubview(self.panoramicImage)
        self.view.sendSubviewToBack(self.panoramicImage)
        
        // Set location API
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.headingFilter = 1
        self.locationManager.distanceFilter = 1
        self.locationManager.delegate = self
        let status = CLLocationManager.authorizationStatus()
        if status == .NotDetermined || status == .Denied {
            self.locationStatusLabel.text = "Status de localización: Solicitando"
            self.locationManager.requestWhenInUseAuthorization()
        }
        
        // Set zoom action
        self.pinchEvent = UIPinchGestureRecognizer(target: self, action: "zoomManager:")
        self.view.addGestureRecognizer(self.pinchEvent)
        
        // Drag event
        self.panEvent = UIPanGestureRecognizer(target: self, action: "dragManager:")
        self.view.addGestureRecognizer(self.panEvent)
        
        // Activate gestures
        self.view.userInteractionEnabled = true
        self.view.multipleTouchEnabled = true
    }
    
    func zoomManager(recognizer: UIPinchGestureRecognizer) {
        var scale = self.zoom * recognizer.scale
        scale = (scale > 0.6 ? scale : 0.6)
        scale = (scale < 3.0 ? scale : 3.0)
        
        // event state
        if recognizer.state == UIGestureRecognizerState.Ended {
            if(scale < 1) {
                self.zoom = 1.0
                UIView.animateWithDuration(0.2, animations: {
                    self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
                })
                return
            }
            self.zoom = scale
        }
        // scale
        var transform = CGAffineTransformMakeScale(scale, scale)
        self.view.transform = transform
    }
    
    func dragManager(manager: UIPanGestureRecognizer) {
        var translation = manager.translationInView(self.view)
        self.panoramicImage.center = CGPointMake(self.panoramicImage.center.x + translation.x, (UIScreen.mainScreen().bounds.size.height / 2.0))
        manager.setTranslation(CGPointZero, inView: self.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func locationManager(manager: CLLocationManager!, didUpdateHeading newHeading: CLHeading!) {
        if self.useCompass {
            self.locationHeadingLabel.text = String(format: "%.0f", Float(manager.heading.trueHeading))
            self.goToAngle(Float(manager.heading.trueHeading))
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        if self.longitude != 0.0 && self.latitude != 0.0 {
            var dLatitude = (self.latitude - Float(newLocation.coordinate.latitude)) * pow(10, 4)
            var dLongitude = (self.longitude - Float(newLocation.coordinate.longitude)) * pow(10, 4)
            self.locationDiference.text = String(format: "(%.8f, %.8f)", dLatitude, dLongitude)
        }
        self.locationPositionLabel.text = String(format: "(%.8f, %.8f)", newLocation.coordinate.latitude, newLocation.coordinate.longitude)
        self.latitude = Float(newLocation.coordinate.latitude)
        self.longitude = Float(newLocation.coordinate.longitude)
    }
    
    func locationManagerShouldDisplayHeadingCalibration(manager: CLLocationManager!) -> Bool {
        return (manager.heading.headingAccuracy < 0 || manager.heading.headingAccuracy > 5)
    }
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            self.locationStatusLabel.text = "Status de localización: Autorizado"
            self.locationManager.startUpdatingHeading()
            self.locationManager.startUpdatingLocation()
        } else {
            self.locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        
    }
    
    @IBAction func move(sender: AnyObject) {
        self.useCompass = !self.useCompass
    }
    
    @IBAction func pick(sender: AnyObject) {
        self.imagePickerController = UIImagePickerController()
        self.imagePickerController.delegate = self
        self.imagePickerController.sourceType = .Camera
        self.presentViewController(self.imagePickerController, animated: true, completion: nil)
    }
    
}

